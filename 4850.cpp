#include <iostream>
#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>

using namespace std;

int sum_n(int n) { 
    int res = n / 1000 + (n % 1000 - n % 100) / 100 + (n % 100 - n % 10) / 10 + n % 10;
    return res;
}

bool was[10000]; 
int vertex[10000]; 
queue<int> q; 

void bfs(int start, int finish) {
    if (start == finish) { 
        cout << 0 << endl;
        return;
    }
    else {
        for (int i = 0; i < 10000; i++) { 
            vertex[i] = 1;
        }
        q.push(start); 
        was[start] = true;
        while (!q.empty()) {
            int tmp = q.front(); 
            q.pop();
            int tmp1 = tmp;
            tmp1 *= 3; 
            if (!was[tmp1]) {
                if (tmp1 == finish) {
                    cout << vertex[tmp] << endl;
                    return;
                }
                else if (tmp1 < 10000 && tmp1 >= 0) {
                    was[tmp1] = true;
                    q.push(tmp1);
                    vertex[tmp1] = vertex[tmp] + 1;
                }
            }
            tmp1 = tmp;
            tmp1 += sum_n(tmp1);
            if (!was[tmp1]) {
                if (tmp1 == finish) {
                    cout << vertex[tmp] << endl;
                    return;
                }
                else if (tmp1 < 10000 && tmp1 >= 0) {
                    was[tmp1] = true;
                    vertex[tmp1] = vertex[tmp] + 1;
                    q.push(tmp1);
                }
            }
            tmp1 = tmp;
            tmp1 -= 2;
            if (!was[tmp1]) {
                if (tmp1 == finish) {
                    cout << vertex[tmp] << endl;
                    return;
                }
                else if (tmp1 < 10000 && tmp1 >= 0) {
                    was[tmp1] = true;
                    vertex[tmp1] = vertex[tmp] + 1;
                    q.push(tmp1);
                }
            }
        }
    }
}

int main() {
    int a, b;
    cin >> a >> b; 
    bfs(a, b);
    return 0;
}