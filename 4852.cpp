#include <iostream>
#include <queue>
using namespace std;
int graph[1001][1001], d[1001];
queue <int> q;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, x, v;
    cin >> n >> x;
    x--;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> graph[i][j];
        }
    }
    for (int i = 0; i < n; i++) {
        d[i] = -1;
    }
    q.push(x);
    d[x] = 0;
    while (!q.empty()) {
        v = q.front();
        q.pop();
        for (int i = 0; i < n; i++) {
            if (d[i] == -1 && graph[v][i]) {
                q.push(i);
                d[i] = d[v] + 1;
            }
        }
    }
    for (int i = 0; i < n; i++) {
        cout << d[i] << " ";
    }
    return 0;
}