#include <iostream>
#include <string>
#include <cmath>
using namespace std;

struct cell {
    int a;  
    int b;  
    int d;  
};

struct node {
    cell val;
    node* next;
    node(cell v, node* n = NULL) {
        val = v;
        next = n;
    }
};

struct Queue {
    node* head;
    Queue(node* h = NULL) {
        head = h;
    }
    void push(cell v) { 
        head = new node(v, head);
    }
    cell pop() {    
        if ((head->next) != NULL) {
            node* i = head;
            node* p = i;
            while (i->next != NULL) {
                p = i;
                i = i->next;
            }
            cell v = i->val;
            p->next = NULL;
            delete i;
            return v;
        }
        else {
            node* n = head;
            cell v = head->val;
            head = head->next;
            delete n;
            return v;
        }
    }
    void clear() {  
        head = NULL;
    }
};

int main() {
    string r;  
    Queue q;   
    cell current, goal, next;   
    bool visited[8][8]; 
    while (getline(cin, r)) {

        current.a = (int)(r[1] - '1');
        current.b = (int)(r[0] - 'a');
        goal.a = (int)(r[4] - '1');
        goal.b = (int)(r[3] - 'a');
        current.d = 0;

        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                visited[i][j] = false;
        visited[current.a][current.b] = true;
        
        q.push(current);
       
        while (!(current.a == goal.a && current.b == goal.b)) {   
            current = q.pop();  
            next.d = current.d + 1; 
            for (int i = -2; i <= 2; i++)
                if (i != 0)
                    for (int j = -(3 - abs(i)); j <= 3 - abs(i); j += 2 * (3 - abs(i))) {
                       
                        next.a = current.a + i;
                        next.b = current.b + j;
                        if (0 <= next.a && next.a <= 7 && 0 <= next.b && next.b <= 7 && !visited[next.a][next.b]) {  
                            q.push(next);  
                            visited[next.a][next.b] = true;
                        }
                    }
        }


        cout << "To get from " << r[0] << r[1] << " to " << r[3] << r[4] << " takes " << current.d << " knight moves." << endl;

        q.clear();
    }
    return 0;
}