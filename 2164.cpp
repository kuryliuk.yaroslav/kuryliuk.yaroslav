#include <iostream>
#include <cstring>
using namespace std;

int main() {
	const int MAX_SIZE = 256; 
	char* cipher = new char[MAX_SIZE];
	cin.getline(cipher, MAX_SIZE);
	int k;
	cin >> k;
	for (int i = 0; i < strlen(cipher); i++) { 
		if (cipher[i] >= 'A' + k) cipher[i] -= k;
		else cipher[i] = 'Z' - (k - (cipher[i] - '@')); 
	}
	cout << cipher;
	return 0;
}